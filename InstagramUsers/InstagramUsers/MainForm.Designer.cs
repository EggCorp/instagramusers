﻿namespace InstagramUsers
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtFolderInput = new System.Windows.Forms.TextBox();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.btnCheckEnglishName = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folder Input:";
            // 
            // txtFolderInput
            // 
            this.txtFolderInput.Enabled = false;
            this.txtFolderInput.Location = new System.Drawing.Point(84, 22);
            this.txtFolderInput.Name = "txtFolderInput";
            this.txtFolderInput.Size = new System.Drawing.Size(201, 20);
            this.txtFolderInput.TabIndex = 1;
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(307, 20);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(94, 23);
            this.btnSelectFolder.TabIndex = 2;
            this.btnSelectFolder.Text = "Select Folder";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // btnCheckEnglishName
            // 
            this.btnCheckEnglishName.Location = new System.Drawing.Point(15, 72);
            this.btnCheckEnglishName.Name = "btnCheckEnglishName";
            this.btnCheckEnglishName.Size = new System.Drawing.Size(156, 23);
            this.btnCheckEnglishName.TabIndex = 3;
            this.btnCheckEnglishName.Text = "Check English Name";
            this.btnCheckEnglishName.UseVisualStyleBackColor = true;
            this.btnCheckEnglishName.Click += new System.EventHandler(this.btnCheckEnglishName_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 289);
            this.Controls.Add(this.btnCheckEnglishName);
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.txtFolderInput);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Instagram Users";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFolderInput;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.Button btnCheckEnglishName;
    }
}

