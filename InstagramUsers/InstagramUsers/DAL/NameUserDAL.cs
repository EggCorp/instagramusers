﻿using InstagramUsers.Model;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace InstagramUsers.DAL
{
    public static class NameUserDAL
    {
        public static bool IsExit(int userId, int nameId)
        {
            bool result = false;
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.AppSettings["connectionString"].ToString()))
            {
                connection.Open();
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM nameusers where `name_id`=@name_id and `user_id` = @user_id limit 1", connection))
                {
                    command.Parameters.AddWithValue("@name_id", nameId);
                    command.Parameters.AddWithValue("@user_id", userId);
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result = true;
                        };
                    }
                    connection.Close();
                }
            }
            return result;
        }

        public static void Add(NameUser nameUser)
        {
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.AppSettings["connectionString"].ToString()))
            {
                connection.Open();
                using (MySqlCommand command = new MySqlCommand(
                            @"INSERT INTO  nameusers (`name_id`, `user_id`) VALUES (@name_id, @user_id)", connection))
                {
                    command.Parameters.AddWithValue("@name_id", nameUser.NameId);
                    command.Parameters.AddWithValue("@user_id", nameUser.UserId);

                    command.ExecuteScalar();
                }
                connection.Close();
            }
        }
    }
}
