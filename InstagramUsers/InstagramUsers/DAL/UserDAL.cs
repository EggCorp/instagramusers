﻿using InstagramUsers.Model;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace InstagramUsers.DAL
{
    public static class UserDAL
    {
        public static User GetByUid(string uid)
        {
            User result = null;
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.AppSettings["connectionString"].ToString()))
            {
                connection.Open();
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM users where `uid`=@uid limit 1", connection))
                {
                    command.Parameters.AddWithValue("@uid", uid);
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result = new User();
                            result.Id = reader.GetInt32(reader.GetOrdinal("id"));
                            result.Name = reader.GetString(reader.GetOrdinal("name"));
                        };
                    }
                    connection.Close();
                }
            }
            return result;
        }

        public static User Add(User user)
        {
            int resultId = 0;
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.AppSettings["connectionString"].ToString()))
            {
                connection.Open();
                using (MySqlCommand command = new MySqlCommand(
                            @"INSERT INTO  users (`name`, `uid`) VALUES (@name, @uid)", connection))
                {
                    command.Parameters.AddWithValue("@name", user.Name);
                    command.Parameters.AddWithValue("@uid", user.Uid);

                    command.ExecuteScalar();
                }


                object ores = MySqlHelper.ExecuteScalar(
                connection,
                "SELECT LAST_INSERT_ID();");
                if (ores != null)
                {
                    // Odd, I got ulong here.
                    ulong qkwl = (ulong)ores;
                    resultId = (int)qkwl;
                }
                connection.Close();
            }
            user.Id = resultId;
            return user;
        }
    }
}
