﻿using InstagramUsers.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstagramUsers.DAL
{
    public static class NameDAL
    {
        public static Name GetByName(string name)
        {
            Name result = null;
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.AppSettings["connectionString"].ToString()))
            {
                connection.Open();
                using (MySqlCommand command = new MySqlCommand("SELECT * FROM names where `name`=@name limit 1", connection))
                {
                    command.Parameters.AddWithValue("@name", name);
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result = new Name();
                            result.Id = reader.GetInt32(reader.GetOrdinal("id"));
                            result.DisplayName = reader.GetString(reader.GetOrdinal("name"));
                        };
                    }
                    connection.Close();
                }
            }
            return result;
        }
    }
}
