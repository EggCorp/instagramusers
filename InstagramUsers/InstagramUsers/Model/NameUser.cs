﻿namespace InstagramUsers.Model
{
    public class NameUser
    {
        public int Id { get; set; }
        public int NameId { get; set; }
        public int UserId { get; set; }
    }
}
