﻿namespace InstagramUsers.Model
{
    public class Name
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
    }
}
