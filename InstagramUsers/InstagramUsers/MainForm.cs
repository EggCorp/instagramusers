﻿using InstagramUsers.DAL;
using InstagramUsers.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstagramUsers
{
    public partial class MainForm : Form
    {
        string _folderDone = string.Empty;
        string _folderResult = string.Empty;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            DialogResult result = folderDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFolderInput.Text = folderDialog.SelectedPath;
            }
        }

        private async void btnCheckEnglishName_Click(object sender, EventArgs e)
        {
            try
            {
                btnCheckEnglishName.Enabled = false;

                _folderDone = string.Format("{0}\\Done", txtFolderInput.Text);
                if (!Directory.Exists(_folderDone))
                    Directory.CreateDirectory(_folderDone);

                _folderResult = string.Format("{0}\\Result", txtFolderInput.Text);
                if (!Directory.Exists(_folderResult))
                    Directory.CreateDirectory(_folderResult);

                var allFiles = Directory.GetFiles(txtFolderInput.Text);

                await Task.Run(() =>
                {
                    foreach (string file in allFiles)
                    {
                        string[] lines = File.ReadAllLines(file);
                        List<string> resultUser = new List<string>();
                        foreach (string line in lines)
                        {
                            if (CheckEnglishName(line))
                                resultUser.Add(line);
                        }

                        // move file done
                        FileInfo fileInfo = new FileInfo(file);
                        System.IO.File.Move(fileInfo.FullName, string.Format("{0}/{1}", _folderDone, fileInfo.Name));

                        // result file
                        string fileResult = string.Format("{0}/{1}", _folderResult, fileInfo.Name);
                        File.WriteAllLines(fileResult, resultUser.ToArray());
                    }
                });

                InforMessage("Finish Check Instagram User Name!");
            }
            catch(Exception ex)
            {
                ErrorMessage(ex.Message);
            }
            finally
            {
                btnCheckEnglishName.Enabled = true;
            }
            
        }
        private void ErrorMessage(string message)
        {
            Invoke(new Action(() =>
            {
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }));

        }

        private void InforMessage(string message)
        {
            Invoke(new Action(() =>
            {
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }));
        }

        private bool CheckEnglishName(string input)
        {
            string[] argInput = input.Split('|');
            string uid = argInput[0];
            
            bool result = false;
            if (argInput.Count() > 1)
            {
                string instagramUserName = argInput[1];
                string[] words = GetWords(instagramUserName);
                foreach(string word in words)
                {
                    Name name = NameDAL.GetByName(word);
                    if (name != null)
                    {
                        result = true;
                        User user = UserDAL.GetByUid(uid);
                        if (user == null)
                            user = UserDAL.Add(new User { Uid = uid, Name = instagramUserName });
                        if (!NameUserDAL.IsExit(user.Id, name.Id))
                            NameUserDAL.Add(new NameUser { NameId = name.Id, UserId = user.Id });
                    }
                }
            }

            return result;
        }

        static string[] GetWords(string input)
        {
            MatchCollection matches = Regex.Matches(input, @"\b[\w']*\b");

            var words = from m in matches.Cast<Match>()
                        where !string.IsNullOrEmpty(m.Value)
                        select TrimSuffix(m.Value);

            return words.ToArray();
        }

        static string TrimSuffix(string word)
        {
            int apostropheLocation = word.IndexOf('\'');
            if (apostropheLocation != -1)
            {
                word = word.Substring(0, apostropheLocation);
            }

            return word;
        }
    }
}
